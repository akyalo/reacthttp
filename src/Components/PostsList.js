import React, { Component } from 'react'
import axios  from 'axios'


 class PostsList extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
             posts:[],
             errorMsg:[]
              
         }
     }

     componentDidMount(){
         axios.get('https://jsonplaceholder.typicode.com/posts/1/comments')
           .then(response => { 
             console.log(response)
             this.setState({posts:response.data})
            } )

         .catch(error=> {
             console.log(error)
             this.setState({errorMsg: 'Error Retriving Data '})
                })
     }
     
    render() {
        const { posts , errorMsg } = this.state


        return (
            <div>
               The   List of  Posts  
               {
                   posts.length ?
                   posts.map(post => <div key =  {post.id} > {post.body}   &nbsp;  &nbsp;  &nbsp;   {post.name }   &nbsp;  &nbsp;  &nbsp;    {post.email}  </div>) : 
                   null

                }
                {errorMsg?<div>{errorMsg}</div>:null}
                
            </div>
        )
    }
}

export default PostsList
